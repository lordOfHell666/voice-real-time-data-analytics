import { Injectable } from '@angular/core';
import { Observable, observable, Subscriber } from 'rxjs';
import {io} from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  webSocket:any
  wsid:any
readonly url:string = environment.apiUrl

constructor() {
  this.webSocket = io(this.url)
 }


 startConnection() {
  this.webSocket = io(this.url, { transports: ['websocket'] });
  this.webSocket.on('connect', () => {
    console.log('%cSocket Connected on ' + this.webSocket.io.opts.hostname + this.webSocket.io.opts.path, 'color: brown; font-weight: bold;');
    this.wsid = this.webSocket.id;
  });
}

 listen(eventName:string){
return new Observable((subscriber) => {
  this.webSocket.on(eventName, (data:any)=> {
    subscriber.next(data);
      }) 
    });
 }

emit(eventName: string, data:any){
  this.webSocket.emit(eventName, data)
}


}
