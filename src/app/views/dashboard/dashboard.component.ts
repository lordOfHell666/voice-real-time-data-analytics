import { Component, Input, OnInit, ChangeDetectionStrategy,ChangeDetectorRef, ViewRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SocketService } from 'src/app/socket-service/socket.service';
import { ColDef,} from 'ag-grid-community';
import { DashboardChartsData, IChartProps } from './dashboard-charts-data';
import { TimerComponent } from './timer/timer.component';
import { interval, Subject } from 'rxjs';
import { filter, startWith, takeUntil } from 'rxjs/operators';

// interface IUser {
//   name: string;
//   state: string;
//   registered: string;
//   country: string;
//   usage: number;
//   period: string;
//   payment: string;
//   activity: string;
//   avatar: string;
//   status: string;
//   color: string;
// }
export interface TimeSpan {
  hours: number;
  minutes: number;
  seconds: number;
}

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
  gridApi:any;
  queueGrid:any;
  chkfn:any;
  day:number = 0 ;
  hour:number = 0 ;
  min:number = 0 ;
  second:number = 0;
  gridColumnApi:any;
  rowSelection:any;
  autoGroupColumnDef:any;
  statusBar:any;
  groupDefaultExpanded:any;
  getRowNodeId:any;
  @Input() tableData:any = [];
  paginationPageSize1 = 10;
  paginationPageSize2 = 10;
  p1: number = 1;
  p2: number = 1;
  collection: any = []; 
  defaultColDef:any = {
    sortable: true,
  };
  searchText:any = ''
  searchQueuedText:any = ''
  rowHeight: any = 40;
  frameworkComponents = {
    TimeCellRenderer: 'TimerComponent',
  };;
  columnDefs:ColDef[] = [{
    headerName:'Queue Phone No',                     
    field: "customer_no",
    flex:1
    }, { 
    headerName:'Queue Wait time', 
    field: "agent.agent_name",
    flex:1
    }, { 
    headerName:'Queue/Team Name',
    field: "voice_queue",
    flex:1
    },];

  rowColumnDefs:ColDef[]=[{
    headerName:'Queue Phone No',                     
    field: "customer_no",
    flex:1
    }, { 
    headerName:'Queue Wait time', 
    field: "agent.agent_name",
    flex:1
    }, { 
    headerName:'Queue/Team Name',
    field: "voice_queue",
    flex:1
    },
  ]
  agentOverviewValue:any = [{
    abandoned_calls : 'NA',
    agents_break : 'NA',
    agents_idle : 'NA',
    agents_online : 'NA',
    assigned_calls : 'NA',
    avg_handling_time : 'NA',
    avg_wait_time : 'NA',
    call_utilization : 'NA',
    calls_not_replied : 'NA',
    not_answered : 'NA',
    ongoing_calls : 'NA',
    queue_name : 'NA',
    queued_calls : 'NA',
    total_calls : 'NA'
  }]
  rowData:any = [];
  updatedRowData:any  = []
  queuedRowData:any = []
  private destroyed$ = new Subject<void>();
  constructor(private socket:SocketService, private changeDetector:ChangeDetectorRef) {
    this.getRowNodeId = function (data:any) {
      console.log("data",data)
      return data.unique_id;
    };
  }
  ngOnInit(): void {
    this.socket.startConnection()
    // this.getTabularData()
    this.getData()
    this.getQueuedData('')
    console.log("agentOverviewValue==>",this.agentOverviewValue)
    this.socket.listen('voice_realtime_update').subscribe((data:any)=>{
      var realTimeUpdate = JSON.parse(data);
      console.log("real-time-data",data)
      for(let key in JSON.parse(data)){
        if(key == 'avg_wait_time'){
          if(realTimeUpdate[key] <= '120'){
            this.agentOverviewValue[0][key] = `${1}h ${1}m  ${0}s`   
          }  else if(realTimeUpdate[key] <= '60'){
            this.agentOverviewValue[0][key] = `${1}m ${0}s`   
          } else {
            this.agentOverviewValue[0][key] = `${realTimeUpdate[key]}s`   
          }
        } else {
          this.agentOverviewValue[0][key] = realTimeUpdate[key]
        }
        console.log(this.agentOverviewValue)
        
        }
    })

    interval(1000).subscribe(() => {
      if (!(this.changeDetector as ViewRef)['destroyed']) {
        this.changeDetector.detectChanges();
      }
    });

    this.changeDetector.detectChanges();
  }


  // getAvgWaitTime(data:any){
  //   if(data == 60){
  //       this.min = this.min + 1;
  //       this.second = 0;
  //     }
  //     if (this.min == 60) {
  //       this.hour = this.hour + 1;
  //       this.min = 0;
  //       sec = 0;
  //     }
  // }

  getQueuedData(agentCallData:any){
    this.socket.listen('voice_queue_message').subscribe((data:any)=>{
      if(JSON.parse(data).status == 'queued'){
        if(this.queuedRowData.length == 0){
          this.queuedRowData.push(JSON.parse(data))
        }
      } 
      console.log("getQueuedData",this.queuedRowData)
      let eventData = {
        flag:false,
        incomingData:JSON.parse(data),
        originalData:{},
        index:0
      };
      for(let i = 0,l= this.queuedRowData.length;i<l;i++){
        if(this.queuedRowData[i].unique_id == JSON.parse(data).unique_id){
          eventData.flag = true;
          eventData.originalData = this.queuedRowData[i];
          eventData.index = i
          break;
        } else {
          eventData.flag = false;
          eventData.originalData = {};
        }
      }
      if(eventData.flag){
        this.queuedRowData[eventData.index].status = eventData.incomingData.status
        // // this.queuedRowData.splice(0,eventData.index+1)
        // this.queuedRowData[eventData.index].status != 'queued' ? this.queuedRowData.splice(0,eventData.index) : ''
      } else {
        this.queuedRowData.push(eventData.incomingData)
      } 
      for(var i = 0,l = this.queuedRowData.length;i<l;i++){
        if(this.queuedRowData[i].status != 'queued'){
          this.queuedRowData.splice(i, 1);
        }
      }
    })
  }


//   getTabularData(){
    
//     this.socket.listen('voice_message').subscribe((data:any)=>{
//       console.log("socketData",JSON.parse(data))
//       // //       if(JSON.parse(data)){
//       // //   this.updateEvenRowData(data)

//       // // }
//       if(this.updatedRowData.length == 0){
//         this.getInitialData(JSON.parse(data))
//       } else {
//       // for(let i = 0,l= this.updatedRowData.length;i<l;i++){
//       //   if(this.updatedRowData[i].unique_id == JSON.parse(data).unique_id){
//       //     this.updatedRowData[i].status = JSON.parse(data).status
//       //     this.updatedRowData[i].agent.agent_name = JSON.parse(data).agent.name
//       //     this.updatedRowData[i].agent.agent_email_id = JSON.parse(data).agent.agent_email_id
//       //     if(JSON.parse(data).status == 'Inbound'){
//       //       this.updatedRowData[i].call_duration = `${this.getElapsedTime(JSON.parse(data))}`
//       //     }
//       //     console.log("test",this.updatedRowData[i],JSON.parse(data))
//       //   } else {
//       //     this.getInitialData(JSON.parse(data))
//       //   }
//       // }
//           // this.rowData.push(JSON.parse(data))
//       // console.log("this.rowData",this.rowData)
//     // })
//   }
// }
//   )}


getData(){
      this.socket.listen('voice_message').subscribe((data:any)=>{
        if(this.updatedRowData.length == 0){
          this.updatedRowData.push(JSON.parse(data))
          console.log("hello===>",this.updatedRowData)
        }
        let eventData = {
          flag:false,
          incomingData:JSON.parse(data),
          originalData:{},
          index:0
        };
        for(let i = 0,l= this.updatedRowData.length;i<l;i++){
          this.getQueuedData(this.updatedRowData[i])
          if(this.updatedRowData[i].agent.agent_name == JSON.parse(data).agent.agent_name){
            eventData.flag = true;
            eventData.originalData = this.updatedRowData[i];
            eventData.index = i
            break;
          } else {
            eventData.flag = false;
            eventData.originalData = {};
          }
        }


        // if(this.updatedRowData[i].unique_id == JSON.parse(data).unique_id){
        //   this.updatedRowData[i].status = JSON.parse(data).status
        //   if(JSON.parse(data).status == 'inbound'){
        //     this.updatedRowData[i].agent.agent_name = JSON.parse(data).agent.name
        //   } else if(JSON.parse(data).status == 'completed'){
        //     this.updatedRowData[i].call_duration = this.updatedRowData[i].status_time;
        //     this.updatedRowData[i].call_duration = 0
        //   }
        //   console.log("test",this.updatedRowData[i],JSON.parse(data))
        //   break;
        // }
        if(eventData.flag){
          this.updatedRowData[eventData.index].status = eventData.incomingData.status
          this.updatedRowData[eventData.index].start_time = eventData.incomingData.start_time
          this.updatedRowData[eventData.index].work_status = eventData.incomingData.work_status
          this.updatedRowData[eventData.index].voice_queue = eventData.incomingData.voice_queue
          this.updatedRowData[eventData.index].work_status_time = eventData.incomingData.work_status_time
          this.updatedRowData[eventData.index].agent.agent_name = JSON.parse(data).agent.agent_name
          this.updatedRowData[eventData.index].customer_no = JSON.parse(data).customer_no
          console.log("this.updatedRowData[eventData.index]",this.updatedRowData[eventData.index])
        } else {
          this.updatedRowData.push(eventData.incomingData)
        } 
      })
}

  getOfflineTime(entry: any) { 
    let statusFlag = entry.work_status == 'idle' || entry.status == 'offline' || entry.status == 'break' || entry.status == 'hold'? true : false
    let totalSeconds = Math.floor((new Date().getTime() - new Date(entry.work_status_time).getTime()) / 1000);
    
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    if (totalSeconds >= 3600) {
      hours = Math.floor(totalSeconds / 3600);      
      totalSeconds -= 3600 * hours;      
    }

    if (totalSeconds >= 60) {
      minutes = Math.floor(totalSeconds / 60);
      totalSeconds -= 60 * minutes;
    }

    seconds = totalSeconds;
    console.log("hour===>",entry)
    if(statusFlag){    
    return {
      hours: hours,
      minutes: minutes,
      seconds: seconds
    };
  } else {
    console.log(entry)
    return {
      hours: '--',
      minutes: '--',
      seconds: '--'
    };
  }
  }


  getElapsedTime(entry: any) { 
    let statusFlag = entry.work_status == 'on_call'? true : false
    let totalSeconds = Math.floor((new Date().getTime() - new Date(entry.start_time).getTime()) / 1000);
    
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    if (totalSeconds >= 3600) {
      hours = Math.floor(totalSeconds / 3600);      
      totalSeconds -= 3600 * hours;      
    }

    if (totalSeconds >= 60) {
      minutes = Math.floor(totalSeconds / 60);
      totalSeconds -= 60 * minutes;
    }

    seconds = totalSeconds;
    console.log("hour===>",entry)
    if(statusFlag){    
    return {
      hours: hours,
      minutes: minutes,
      seconds: seconds
    };
  } else {
    console.log(entry)
    return {
      hours: '--',
      minutes: '--',
      seconds: '--'
    };
  }
  }


  getQueuedElapsedTime(entry: any) { 
    let statusFlag = entry.status == 'queued' ? true : false
    let totalSeconds = Math.floor((new Date().getTime() - new Date(entry.start_time).getTime()) / 1000);
    
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    if (totalSeconds >= 3600) {
      hours = Math.floor(totalSeconds / 3600);      
      totalSeconds -= 3600 * hours;      
    }

    if (totalSeconds >= 60) {
      minutes = Math.floor(totalSeconds / 60);
      totalSeconds -= 60 * minutes;
    }

    seconds = totalSeconds;
    console.log("hour===>",entry)
    if(statusFlag){    
    return {
      hours: hours,
      minutes: minutes,
      seconds: seconds
    };
  } else {
    console.log(entry)
    return {
      hours: '--',
      minutes: '--',
      seconds: '--' 
    };
  }
  }


  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }


  updateEvenRowData(data:any){
    // console.log(data)
    // this.gridApi.setRowData({ add: data });
    this.rowData.push(data)
    this.changeRowDataEvent('',data)
  };

  changeRowDataEvent(event:any,data:any){
    this.gridApi = event.api;
    this.gridColumnApi = event.columnApi;
    event.api.setRowData(data);
  }
  changeQueueRowDataEvent(event:any,data:any){
    
  }
  // updatePrices() {
  //   var immutableStore 
  //   var newStore :any= [];
  //   immutableStore.forEach(function (item) {
  //     newStore.push({
  //       symbol: item.symbol,
  //       group: item.group,
  //       price: Math.floor(Math.random() * 100),
  //     });
  //   });
  //   immutableStore = newStore;
  //   this.gridApi.setRowData(immutableStore);
  // }


}

