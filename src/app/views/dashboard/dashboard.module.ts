import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import {
  AvatarModule,
  ButtonGroupModule,
  ButtonModule,
  CardModule,
  FormModule,
  GridModule,
  NavModule,
  ProgressModule,
  TableModule,
  TabsModule
} from '@coreui/angular';
import { IconModule } from '@coreui/icons-angular';
import { ChartjsModule } from '@coreui/angular-chartjs';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import { WidgetsModule } from '../widgets/widgets.module';
import { TimerComponent } from './timer/timer.component';
import { SearchFilterPipe } from 'src/app/pipes/search-filter.pipe';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    DashboardRoutingModule,
    CardModule,
    NavModule,
    IconModule,
    TabsModule,
    FormsModule,
    CommonModule,
    GridModule,
    ProgressModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
    ButtonModule,
    FormModule,
    ButtonModule,
    ButtonGroupModule,
    ChartjsModule,
    AvatarModule,
    NgxPaginationModule,
    TableModule,
    WidgetsModule,
    Ng2SearchPipeModule
  ],
  declarations: [DashboardComponent, TimerComponent, SearchFilterPipe],
  entryComponents:[TimerComponent]
})

export class DashboardModule {
}
