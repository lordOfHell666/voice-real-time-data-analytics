import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BadgeModule, CardModule, GridModule } from '@coreui/angular';
import { ChartjsModule } from '@coreui/angular-chartjs';

import { ChartsComponent } from './charts.component';
import { ChartsRoutingModule } from './charts-routing.module';
import { ComponentsModule } from '../../../components/components.module';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [ChartsComponent],
  imports: [
    CommonModule,
    ChartsRoutingModule,
    ChartjsModule,
    CardModule,
    AgGridModule.withComponents([]),
    GridModule,
    BadgeModule,
    ComponentsModule
  ]
})
export class ChartsModule {
}
