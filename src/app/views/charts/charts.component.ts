import { Component, Input, OnInit } from '@angular/core';
import { ColDef} from 'ag-grid-community';
import { SocketService } from 'src/app/socket-service/socket.service';
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {

  gridApi:any;
  gridColumnApi:any;
  defaultColDef:any;
  rowSelection:any;
  autoGroupColumnDef:any;
  statusBar:any;
  groupDefaultExpanded:any;
  getRowNodeId:any;
  @Input() tableData:any = [];
  columnDefs:ColDef[] = [{    
    headerName:'customer_no',                     
    field: "customer_no"
                         }, { 
                          headerName:'agent_name', 
                          field: "agent.agent_name"
                            }, { 
                              headerName:'agent_email_id',
                              field: "agent.agent_email_id"
                               },
                                  { 
                                 headerName:'start_time',
                                    field: "start_time"
                                  },
                                  {
                                    headerName:'status',
                                    field: "status"
                                  },
                                    {
                                    headerName: 'live',
                                      field: "live"
                                  },
                                  {
                                    headerName:'voice_queue',
                                    field: "voice_queue"
                                  },
                                  {
                                    headerName:'url',
                                    field: "url"
                                  },
                                  
                                  ];
                                  
                                  rowData:any = [];
  updatedRowData:any  = []
  constructor(private socket:SocketService) {
    this.getRowNodeId = function (data:any) {
      console.log("data",data)
      return data.unique_id;
    };
     }

  ngOnInit(): void {
    this.socket.startConnection()
    this.getTabularData()
  }


  getTabularData(){
    
    this.socket.listen('voice_message').subscribe((data:any)=>{
      console.log("socketData",JSON.parse(data))
      // //       if(JSON.parse(data)){
      // //   this.updateEvenRowData(data)

      // // }
      if(this.updatedRowData.length == 0){
        this.getInitialData(JSON.parse(data))
      } else {
      for(let i = 0,l= this.updatedRowData.length;i<l;i++){
        if(this.updatedRowData[i].unique_id == JSON.parse(data).unique_id){
          this.updatedRowData[i].status = JSON.parse(data).status
          this.updatedRowData[i].agent_name = JSON.parse(data).agent_name
          this.updatedRowData[i].agent_email_id = JSON.parse(data).agent_email_id
          this.gridApi.applyTransaction({ update: this.updatedRowData });
        } else {
          this.getInitialData(JSON.parse(data))
        }
      }
          // this.rowData.push(JSON.parse(data))
      // console.log("this.rowData",this.rowData)
    // })
  }
}
  )}


  getInitialData(data:any){
    this.updatedRowData = [
      data
    ];
    this.gridApi.applyTransaction({
      add: this.updatedRowData,
      addIndex: this.rowData.length == 0 ? this.rowData.length : this.rowData.length+1,
    });
  }

  updateEvenRowData(data:any){
    // console.log(data)
    // this.gridApi.setRowData({ add: data });
    this.rowData.push(data)
    this.changeRowDataEvent('',data)
  };

  changeRowDataEvent(event:any,data:any){
this.gridApi = event.api;
this.gridColumnApi = event.columnApi;
event.api.setRowData(data);
  }
  // updatePrices() {
  //   var immutableStore 
  //   var newStore :any= [];
  //   immutableStore.forEach(function (item) {
  //     newStore.push({
  //       symbol: item.symbol,
  //       group: item.group,
  //       price: Math.floor(Math.random() * 100),
  //     });
  //   });
  //   immutableStore = newStore;
  //   this.gridApi.setRowData(immutableStore);
  // }

}
