import { TestBed } from '@angular/core/testing';

import { RealSocketService } from './real-socket.service';

describe('RealSocketService', () => {
  let service: RealSocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RealSocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
